import firstTask.StringArrayIterator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IteratorTest {

    public static StringArrayIterator<String> iterator;
    public static StringBuilder builder;

    @BeforeAll
    static void prepareData() {
        String[] strArr = new String[]{"Why", "we", "still", "here?", "Just", "to", "suffer?"};
        iterator = new StringArrayIterator<>(strArr);
        builder = new StringBuilder();
    }

    @Test
    public void iterate() {
        while (iterator.hasNext()) {
            builder.append(iterator.next()).append(" ");
        }
        assertEquals("Why we still here? Just to suffer? ", builder.toString());
    }
}
