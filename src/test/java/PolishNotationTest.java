import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import secondTask.ReversePolishRotation;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PolishNotationTest {

    static String INFIX = "4 + ( 3 - 1 ) * 1 + 4 / 2";
    static String POSTFIX = "4 3 1 - 1 * + 4 2 / +";
    static int RESULT = 8;

    @Test
    public void convert() {
        assertEquals(POSTFIX, ReversePolishRotation.infixToPost(INFIX));
    }

    @Test
    public void calculate() {
        assertEquals(RESULT, ReversePolishRotation.calculatePost(POSTFIX));
    }
}
