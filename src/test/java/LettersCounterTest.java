import org.junit.jupiter.api.Test;
import thirdTask.LettersCounter;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LettersCounterTest {

    @Test
    public void count_HelloWorld() {
        assertEquals("{l=3, o=2, r=1, d=1, e=1, w=1, h=1}", LettersCounter.count("Hello World").toString());
    }

    @Test
    public void count_Hyd() {
        assertEquals("{а=2, е=2, к=2, б=1, т=1, у=1, д=1, л=1, я=1}", LettersCounter.count("Как у тебя дела?").toString());
    }
}
