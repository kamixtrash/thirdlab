package secondTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for conversion from infix notation to postfix.
 */
public class ReversePolishRotation {

    public static Map<String, Integer> map = new HashMap<>();

    /**
     * Method to calculate postfix notation expression.
     * @param inputString postfix notation to calculate.
     * @return result of calculation.
     */
    public static int calculatePost(String inputString) {
        String[] input = inputString.split(" ");

        Stack<Integer> stack = new Stack<>();
        for (String character : input) {
            if (isNumber(character)) {
                stack.push(Integer.valueOf(character));
            } else {
                int numberOne = stack.pop();
                int numberTwo = stack.pop();
                int result = 0;
                switch (character) {
                    case "+":
                        result = numberTwo + numberOne;
                        break;
                    case "-":
                        result = numberTwo - numberOne;
                        break;
                    case "*":
                        result = numberTwo * numberOne;
                        break;
                    case "/":
                        result = numberTwo / numberOne;
                        break;
                }
                stack.push(result);
            }
        }
        return stack.pop();
    }

    /**
     * Method to convert from infix notation to postfix.
     * @param infix expression which should be converted.
     * @return expression in postfix notation.
     */
    public static String infixToPost(String infix) {
        map.put("+", 0);
        map.put("-", 0);
        map.put("*", 1);
        map.put("/", 1);
        map.put("(", 2);
        map.put(")", 2);

        Stack<String> stack = new Stack<>();
        String[] chars = infix.split(" ");
        StringBuilder result = new StringBuilder();
        for (String character : chars) {
            if (isNumber(character)) {
                if (result.length() == 0) {
                    result.append(character);
                } else {
                    result.append(" ").append(character);
                }
            } else {
                if (character.equals("(")) {
                    stack.push(character);
                } else {
                    if (character.equals(")")) {
                        String temp;
                        String s1 = "";
                        while (!(temp = stack.pop()).equals("(")) {
                            s1 += " " + temp;
                        }
                        result.append(s1);
                    } else {
                        int priority = getPriority(character);
                        String s1 = "";
                        boolean flag = false;
                        while (!stack.empty()) {
                            flag = false;
                            s1 = stack.pop();
                            if (s1.equals("(")) {
                                break;
                            }
                            if (getPriority(s1) >= priority) {
                                result.append(" ").append(s1);
                                flag = true;
                            }
                        }
                        if (!s1.equals("") && !flag) {
                            stack.push(s1);
                        }
                        stack.push(character);
                    }
                }
            }
        }

        while (!stack.empty()) {
            result.append(" ").append(stack.pop());
        }

        return result.toString();
    }

    /**
     * Method for getting priority of math. symbols.
     * @param character math. symbol.
     * @return priority in Integer type.
     */
    public static int getPriority(String character) {
        return map.get(character);
    }

    /**
     * Method for checking if passed character is number.
     * @param character some value of String type.
     * @return true if character is actual number, else - false.
     */
    public static boolean isNumber(String character) {
        Pattern pattern = Pattern.compile("^(0|[1-9][0-9]*)$");
        Matcher matcher = pattern.matcher(character);
        return matcher.find();
    }
}
