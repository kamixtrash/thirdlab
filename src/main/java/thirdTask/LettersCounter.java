package thirdTask;

import java.util.*;

/**
 * Class for counting characters in String and sorting them by quantity.
 */
public class LettersCounter {

    /**
     * Method for counting and sorting.
     * @param input String in which we should count numbers.
     * @return HashMap sorted by descending.
     */
    public static Map<Character, Integer> count(String input) {

        input = input.toLowerCase();

        Map<Character, Integer> dict = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {
            if ((input.charAt(i) != ' ')
                    && (Character.isAlphabetic(input.charAt(i)))
                    || (Character.isDigit(input.charAt(i)))) {
                if (dict.containsKey(input.charAt(i))) {
                    dict.put(input.charAt(i), dict.get(input.charAt(i))+1);
                } else {
                    dict.put(input.charAt(i), 1);
                }
            }
        }

        List<Map.Entry<Character, Integer>> list = new LinkedList<>(dict.entrySet());
        list.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        Map<Character, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<Character, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
