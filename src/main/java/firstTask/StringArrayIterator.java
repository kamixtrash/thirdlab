package firstTask;

import java.util.Iterator;

/**
 * Simple String iterator.
 * @param <String> type of array.
 */
public class StringArrayIterator<String> implements Iterator<String> {

    final String[] arr;
    private int currentIndex;

    /**
     *
     * @param arr array which will be iterated.
     */
    public StringArrayIterator(String[] arr) {
        if (arr == null) {
            throw new IllegalArgumentException("Array is null.");
        }
        this.arr = arr;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < arr.length;
    }

    @Override
    public String next() {
        return hasNext() ? arr[currentIndex++] : null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Deleting is not supported (yet?).");
    }
}
